//
//  CorrectViewController.swift
//  LeadersofLittleLearners
//
//  Created by Zachary Timberlake on 4/2/18.
//  Copyright © 2018 ASU. All rights reserved.

import UIKit
import AVFoundation

class CorrectViewController: UIViewController {

    var correctAmount: Int?
    var wordNumber: Int?
    var sound = AVAudioPlayer()
    
    @IBOutlet weak var correctLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        do{
            sound = try AVAudioPlayer(contentsOf:URL.init(fileURLWithPath:Bundle.main.path(forResource:"Great Job You Got It Right!", ofType: "m4a")!))
            sound.play()
        }
        
        catch {
            print(error)
        }

        self.correctLabel.text = "Correct, you've gotten \(String(describing: self.correctAmount)) words correct in a row! Tap the top of the screen to continue to the next word, or the bottom of the screen to exit the app."
        
        // Do any additional setup after loading the view.
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "fromCorrect"{
            let des = segue.destination as! ViewController
            des.correctCounter = self.correctAmount!
            des.wordCounter = self.wordNumber! + 1
        }
    }
    
    @IBAction func exitApp(_ sender: Any) {
        exit(0)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
