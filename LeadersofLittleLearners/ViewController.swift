//
//  ViewController.swift
//  LeadersofLittleLearners
//
//  Created by Zachary Timberlake on 3/18/18.
//  Copyright © 2018 ASU. All rights reserved.
//

import UIKit
import Speech

class ViewController: UIViewController, SFSpeechRecognizerDelegate {

    @IBOutlet weak var microphoneButton: UIButton!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var descriptionForWord: UITextView!
    
    var wordArrayEasy = ["CAT", "BAG", "SAW", "JAM", "PIG"]
    var wordCounter = 0
    var correctCounter = 0
    
    private let speechRecognizer = SFSpeechRecognizer(locale: Locale.init(identifier: "en-US"))
    
    private var recognitionRequest: SFSpeechAudioBufferRecognitionRequest?
    private var recognitionTask: SFSpeechRecognitionTask?
    private let audioEngine = AVAudioEngine()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        descriptionForWord.text = "Tap the screen then spell: \(wordArrayEasy[wordCounter%wordArrayEasy.count]) Tap the screen again when you are done."
        
        microphoneButton.isEnabled = false
        
        speechRecognizer?.delegate = self
        
        SFSpeechRecognizer.requestAuthorization { (authStatus) in
            
            var isButtonEnabled = false
            
            switch authStatus {
            case .authorized:
                isButtonEnabled = true
                
            case .denied:
                isButtonEnabled = false
                print("User denied access to speech recognition")
                
            case .restricted:
                isButtonEnabled = false
                print("Speech recognition restricted on this device")
                
            case .notDetermined:
                isButtonEnabled = false
                print("Speech recognition not yet authorized")
            }
            
            OperationQueue.main.addOperation() {
                self.microphoneButton.isEnabled = isButtonEnabled
            }
        }
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "correctSegue"{
            let des = segue.destination as! CorrectViewController
            des.correctAmount = self.correctCounter
            des.wordNumber = self.wordCounter
        }
        if segue.identifier == "wrongSegue"{
            let des = segue.destination as! WrongViewController
            des.correctWord = self.wordArrayEasy[wordCounter%wordArrayEasy.count]
            des.wordNumber = self.wordCounter
        }
    }

    @IBAction func microphoneTapped(_ sender: Any) {
        if audioEngine.isRunning {
            

            audioEngine.stop()
            recognitionRequest?.endAudio()
            microphoneButton.isEnabled = false
            textView.isHidden = true
            if textView.text.contains(wordArrayEasy[wordCounter%wordArrayEasy.count])
            {
                correctCounter += 1
                wordCounter += 1
                
                if correctCounter == 5
                {
                    correctCounter = 0
                    performSegue(withIdentifier: "completedSegue", sender: nil)
                }
                else
                {
                    performSegue(withIdentifier: "correctSegue", sender: nil)
                }
                
            }
            else
            {
                correctCounter = 0
                performSegue(withIdentifier: "wrongSegue", sender: nil)
            }
            textView.text = ""
        } else {
            textView.isHidden = false
            startRecording()
            
        }
    }
    
    func startRecording() {
        
        if recognitionTask != nil {
            recognitionTask?.cancel()
            recognitionTask = nil
        }
        
        let audioSession = AVAudioSession.sharedInstance()
        do {
            try audioSession.setCategory(AVAudioSessionCategoryRecord)
            try audioSession.setMode(AVAudioSessionModeMeasurement)
            try audioSession.setActive(true, with: .notifyOthersOnDeactivation)
        } catch {
            print("audioSession properties weren't set because of an error.")
        }
        
        recognitionRequest = SFSpeechAudioBufferRecognitionRequest()
        
        let inputNode = audioEngine.inputNode
        
        guard let recognitionRequest = recognitionRequest else {
            fatalError("Unable to create an SFSpeechAudioBufferRecognitionRequest object")
        }
        
        recognitionRequest.shouldReportPartialResults = true
        
        recognitionTask = speechRecognizer?.recognitionTask(with: recognitionRequest, resultHandler: { (result, error) in
            
            var isFinal = false
            
            if result != nil {
                
                self.textView.text = result?.bestTranscription.formattedString
                isFinal = (result?.isFinal)!
            }
            
            if error != nil || isFinal {
                self.audioEngine.stop()
                inputNode.removeTap(onBus: 0)
                
                self.recognitionRequest = nil
                self.recognitionTask = nil
                
                self.microphoneButton.isEnabled = true
            }
        })
        
        let recordingFormat = inputNode.outputFormat(forBus: 0)
        inputNode.installTap(onBus: 0, bufferSize: 1024, format: recordingFormat) { (buffer, when) in
            self.recognitionRequest?.append(buffer)
        }
        
        audioEngine.prepare()
        
        do {
            try audioEngine.start()
        } catch {
            print("audioEngine couldn't start because of an error.")
        }
        
        
    }
    
    func speechRecognizer(_ speechRecognizer: SFSpeechRecognizer, availabilityDidChange available: Bool) {
        if available {
            microphoneButton.isEnabled = true
        } else {
            microphoneButton.isEnabled = false
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

